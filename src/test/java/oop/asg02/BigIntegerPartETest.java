package oop.asg02;

import org.junit.*;
import static org.junit.Assert.*;

public class BigIntegerPartETest{
	
	@Test
	public void testCompareTo (){

		BigInteger bigInt1 = new BigInteger("11345");
    	BigInteger bigInt2 = new BigInteger("123");

    	assertEquals(1, bigInt1.compareTo(bigInt2));
	}

	@Test
	public void testCompareTo2(){
		BigInteger bigInt1 = new BigInteger("123");
    	BigInteger bigInt2 = new BigInteger("123");

    	assertEquals(0, bigInt1.compareTo(bigInt2));
	}

	@Test
	public void testCompareTo3(){
		BigInteger bigInt1 = new BigInteger("12");
    	BigInteger bigInt2 = new BigInteger("123");

    	assertEquals(-1, bigInt1.compareTo(bigInt2));
	}

	@Test
	public void testCompareToWithZero(){
		BigInteger bigInt1 = new BigInteger("123865365");
		BigInteger bigInt2 = new BigInteger(0);

		assertEquals(1, bigInt1.compareTo(bigInt2));

	}

	@Test 
	public void testClone(){
		BigInteger bigInt1 = new BigInteger("123");
		
    	BigInteger bigInt2 = bigInt1.clone();


    	assertEquals(bigInt1 , bigInt2);
	}
	@Test
	public void testClone2(){
		BigInteger bigInt1 = new BigInteger(133);

    	BigInteger bigInt2 = bigInt1.clone();

    	assertNotNull(bigInt2);
	}
}